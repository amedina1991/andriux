import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class ChatService {
  private url = 'http://localhost:9000';
  private socket;
  constructor(private http: HttpClient) {
    this.socket = io(this.url);
  }
  public sendMessage(message) {
    this.socket.emit('new-message', message);
  }
  public getMessages = () => {
    return Observable.create((observer) => {
      this.socket.on('new-message', (message) => {
        observer.next(message);
      });
    });
  }

  public allUsers() {
    return this.http.get('http://localhost:9000/all').pipe(map(response => response));
  }
  public insertUser( data: any) {
    return this.http.post('http://localhost:9000/add', data).pipe(map(response => response));
  }
}
