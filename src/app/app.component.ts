import { Component, OnInit } from '@angular/core';
import { ChatService } from './chat.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'firstHouse';
  message: string;
  messages: string[] = [];
  nombre: any;
  apellido: any;

  constructor(public chatService: ChatService) {}

  sendMessage() {
    this.chatService.sendMessage(this.message);
    this.message = '';
  }
  showALl() {
    this.chatService.allUsers().
    subscribe(result => {
      console.log(result);
    });
  }

  Insertar() {
    this.chatService.insertUser({ nombre: this.nombre, apellido: this.apellido}).subscribe(result => {
      console.log(result);
    }, error => {
      console.log(error);
    });
  }

  ngOnInit() {
    this.showALl();
    this.chatService
      .getMessages()
      .subscribe((message: string) => {
        this.messages.push(message);
      });
  }

}
